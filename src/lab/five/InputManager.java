package lab.five;

import java.util.Scanner;

public class InputManager {
    public String getUserInput() {
        String input = "";
        System.out.println("Enter text (to end input data type \"end\")");
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                String inputPart = scanner.nextLine();
                if (inputPart.equals("end"))
                    break;
                input += inputPart;
            }
        }
        return input;
    }
}
