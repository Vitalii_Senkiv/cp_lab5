package lab.five;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringProcessor {
    private String getFirstWord(String str) {
        String pattern = "[^\\s]+";
        Pattern firstWordPattern = Pattern.compile(pattern);
        Matcher matcher = firstWordPattern.matcher(str);
        return matcher.find() ? matcher.group() : "";
    }
    public ArrayList<String> getPenultimateWordsInSentences(String text) {
        var penultimateWords = new ArrayList<String>();

        String patternStr = "[а-яА-ЯіІїЇЄєґҐ]+[\\s\\-,:–]+[а-яА-ЯіІїЇЄєґҐ»]+[.!\\?]»?";

        Pattern penultimateWordsPattern = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = penultimateWordsPattern.matcher(text);

        while (matcher.find()) {
            penultimateWords.add(getFirstWord(matcher.group()));
        }

        return penultimateWords;
    }
}