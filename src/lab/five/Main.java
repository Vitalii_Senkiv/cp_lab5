package lab.five;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String input = new InputManager().getUserInput();
        var words = new StringProcessor().getPenultimateWordsInSentences(input);

        if (words.size() > 0) {
            System.out.println("\nFound words:");
            for (String word : words) {
                System.out.println(word);
            }
        } else {
            System.out.println("Cant find any matches");
        }
    }
}