package lab.five;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringProcessorTest {

    @Test
    void testPenultimateWordsInSentences1() {
        var processor = new StringProcessor();

        var words = processor.getPenultimateWordsInSentences("Іван Якович Франко був видатним " +
                "українським письменником.");

        assertEquals(words.get(0), "українським");
    }

    @Test
    void testPenultimateWordsInSentences2() {
        var processor = new StringProcessor();

        var words = processor.getPenultimateWordsInSentences("Іван Якович Франко був видатним " +
                "українським письменником. Людина, що ніколи не була у Івано-Франківську. Народився і жив він на" +
                "Львівщині.");

        assertEquals(words.size(), 3);
    }
    @Test
    void testPenultimateWordsInSentences3() {
        var processor = new StringProcessor();

        var words = processor.getPenultimateWordsInSentences("");

        assertEquals(words.size(), 0);
    }
}